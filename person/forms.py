# -*- coding: utf-8 -*-
from django import forms
from person.models import *


class TeacherCreateForm(forms.Form):


    def __init__(self, values):
        self.name = values.get('name', None)
        self.surname = values.get('surname', None)
        self.dni = values.get('dni', None)
        self.home = values.get('home', None)
        self.degree = values.get('degree', None)
        self.error = ""


    def is_valid(self):
        if self.name and self.surname and self.dni and self.home and self.degree:
            if Teacher.objects.all().filter(dni=self.dni).count():
                self.error = "Teacher already exists with DNI"
                return False
            return True
        self.error = "Todos los campos son obligatorios"
        return False


    def save(self):
        home = Home.objects.get(pk=self.home)
        teacher = Teacher(
            name=self.name,
            surname=self.surname,
            dni=self.dni,
            home=home,
            degree=self.degree)

        teacher.save()
        return teacher


class TeacherUpdateForm(forms.Form):
    def __init__(self, values, id):
        self.name = values.get('name', None)
        self.surname = values.get('surname', None)
        self.dni = values.get('dni', None)
        self.home = values.get('home', None)
        self.degree = values.get('degree', None)
        self.id = id
        self.error = ""


    def is_valid(self):
        if self.name and self.surname and self.dni and self.home and self.degree:
            return True
        self.error = "Todos los campos son obligatorios"
        return False


    def save(self):
        home = Home.objects.get(pk=self.home)
        teacher = Teacher.objects.get(pk=self.id)
        teacher.name = self.name
        teacher.surname = self.surname
        teacher.dni = self.dni
        teacher.degree = self.degree
        teacher.home = home

        teacher.save()
        return teacher


class TeacherDeleteForm(forms.Form):
    def __init__(self, values, id):
        self.id = id
        self.error = ""


    def is_valid(self):
        if Teacher.objects.filter(pk=self.id).exists():
            return True


    def delete(self):
        Teacher.objects.filter(pk=self.id).delete()
