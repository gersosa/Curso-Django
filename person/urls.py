"""tp02_django URL Configuration."""


from django.conf.urls import url
from views import *
app_name = 'person'

urlpatterns = [
    url(r'^teacher/create/$', TeacherCreate.as_view()),
    url(r'^teacher/list/$', TeacherList.as_view(), name='teacher-list'),
    url(r'^teacher/update/(?P<pk>[0-9]+)/$', TeacherUpdate.as_view(), name='teacher-update'),
    url(r'^teacher/delete/(?P<pk>[0-9]+)/$', TeacherDelete.as_view(), name='teacher-delete'),
    url(r'^login/$', Login.as_view()),
    url(r'^student/home/(?P<pk>[0-9]+)/$', HomeStudent.as_view(), name='student-home'),


]
