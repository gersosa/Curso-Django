# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User, Group
from django.db import models
import datetime

# Create your models here.


class City(models.Model):
    name = models.CharField(max_length=30)
    postal_code = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Home(models.Model):
    street = models.CharField(max_length=30)
    number = models.CharField(max_length=30)
    city = models.ForeignKey(City, on_delete=models.CASCADE)

    def __str__(self):
        return self.city.name + ", " + self.street + ", " + self.number


class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    dni = models.IntegerField()
    home = models.ForeignKey(Home, on_delete=models.CASCADE)


    def __str__(self):
        return self.user.username


class Teacher(Person):
    degree = models.CharField(max_length=30)


class Student(Person):
    file = models.IntegerField()


class Course(models.Model):
    name = models.CharField(max_length=30)
    start_date = models.DateField(default=datetime.date.today())
    end_date = models.DateField(default=datetime.date.today())
    student = models.ManyToManyField(Student)
    teacher = models.ManyToManyField(Teacher)


    def __str__(self):
        return self.name


class Career(models.Model):
    name = models.CharField(max_length=30)
    student = models.ManyToManyField(Student)


    def __str__(self):
        return self.name
