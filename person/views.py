# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView, View
from person.models import *
from person.forms import *
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login
from pprint import pprint


# Create your views here.


class TeacherCreate(CreateView):
    model = Teacher
    template_name = 'teacher_create.html'
    form_class = TeacherCreateForm

    def get(self, request, *args, **kwargs):
        ctx = {}
        return render(request, self.template_name, ctx)


    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        ctx = {}
        if form.is_valid():
            form.save()
            ctx['error'] = "Se agrego el T"
            return render(request, self.template_name, ctx)
        else:
            ctx['error'] = "Todos los campos son obligatorios"
            return render(request, self.template_name, ctx)


class TeacherUpdate(UpdateView):
    model = Teacher
    form_class = TeacherUpdateForm
    template_name = 'teacher_update.html'


    def get(self, request, *args, **kwargs):
        pk_update = kwargs['pk']
        ctx = {}
        ctx['teacher'] = Teacher.objects.get(pk=pk_update)
        return render(request, self.template_name, ctx)


    def post(self, request, *args, **kwargs):
        ctx = {}
        form = self.form_class(request.POST, kwargs['pk'])
        if form.is_valid():
            form.save()
            ctx['error'] = "Se modifico"
            return render(request, self.template_name, ctx)
        else:
            ctx['error'] = "Todos los campos son obligatorios"
            return render(request, self.template_name, ctx)


class TeacherList(ListView):
    model = Teacher
    template_name = 'teacher_list.html'


    def get(self, request, *args, **kwargs):
        ctx = {}
        ctx['teachers'] = Teacher.objects.all()
        return render(request, self.template_name, ctx)


class TeacherDelete(DeleteView):
    model = Teacher
    template_name = 'teacher_delete.html'
    form_class = TeacherDeleteForm

    def get(self, request, *args, **kwargs):
        pk_delete = kwargs['pk']
        ctx = {}
        ctx['teacher'] = Teacher.objects.get(pk=pk_delete)
        return render(request, self.template_name, ctx)


    def post(self, request, *args, **kwargs):
        ctx = {}
        form = self.form_class(request.POST, kwargs['pk'])
        if form.is_valid():
            form.delete()
            ctx['error'] = "Se elimino"
            return HttpResponseRedirect(reverse_lazy('person:teacher-list'))


class Login(View):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        return render(request, self.template_name, ctx)


    def post(self, request, *args, **kwargs):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            print user.groups.all().values_list('name', flat=True)
            if 'Teacher' in user.groups.all().values_list('name', flat=True):
                return HttpResponseRedirect(reverse_lazy('person:teacher-list'))
            elif 'Student' in user.groups.all().values_list('name', flat=True):
                print user.pk
                return HttpResponseRedirect(reverse_lazy('person:student-home', kwargs={'pk': user.pk}))


class HomeStudent(ListView):
    template_name = 'student_home.html'


    def get(self, request, *args, **kwargs):
        ctx = {}
        student = Student.objects.get(user=kwargs['pk'])
        print student
        ctx['courses'] = student.course_set.all()
        return render(request, self.template_name, ctx)
