# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from person.models import *

# Register your models here.

admin.site.register(Teacher)
admin.site.register(Student)
admin.site.register(City)
admin.site.register(Home)
admin.site.register(Course)
admin.site.register(Career)
